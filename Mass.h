//
// Created by rerand0m on 05.01.17.
//

#ifndef BIN_TREE_COURSE_PROJECT_MASS_H
#define BIN_TREE_COURSE_PROJECT_MASS_H

template<typename T>
struct Mass
{
    T** m;
    unsigned long size;

    Mass();
    Mass(const Mass&);
    ~Mass();

    void add(T&);
    void adda(T*);
    void del(unsigned long);
    T* operator[](unsigned long);
};

template<typename T>
Mass<T>::Mass() : m(nullptr), size(0) {}

template<typename T>
Mass<T>::Mass(const Mass &a) : size(a.size)
{
    m = new T*[size];
    for(decltype(size) i = 0; i < size; ++i)
        m[i] = new T(*a.m[i]);
}

template<typename T>
Mass<T>::~Mass()
{
    while(size--)
        delete m[size];
    delete[] m;
}

template<typename T>
void Mass<T>::add(T&a)
{
    T** temp = new T*[size+1];
    for(decltype(size) i = 0; i < size; ++i)
        temp[i] = m[i];
    delete[] m;
    temp[size++] = new T(a);
    m = temp;
}
template<typename T>
void Mass<T>::adda(T*a)
{
    T** temp = new T*[size+1];
    for(decltype(size) i = 0; i < size; ++i)
        temp[i] = m[i];
    delete[] m;
    temp[size++] = a;
    m = temp;
}
template<typename T>
void Mass<T>::del(unsigned long n)
{
    if(n < size)
    {
        T **temp = new T *[size - 1];
        for(decltype(size) i = 0; i < size; ++i)
        {
            if(i < n)
                temp[i] = m[i];
            else if(i > n)
                temp[i - 1] = m[i];
        }
        delete m[n];
        delete[] m;
        m = temp;
        --size;
    }
}

template<typename T>
T* Mass<T>::operator[](unsigned long n)
{
    if(n < size)
        return m[n];
    else
        return nullptr;
}

#endif //BIN_TREE_COURSE_PROJECT_MASS_H
