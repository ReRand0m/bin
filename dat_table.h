//
// Created by rerand0m on 15.02.17.
//

#include <iostream>

#ifndef BIN_TREE_COURSE_PROJECT_DAT_TABLE_H
#define BIN_TREE_COURSE_PROJECT_DAT_TABLE_H

struct dat_table_elem
{
    dat_table_elem(std::streampos a, std::streampos b)
    {
        start = a;
        end = b;
    }
    std::streampos start;
    std::streampos end;
};

struct dat_table
{
    unsigned long size;
    struct dat_table_elem** m;

    void add(std::streampos a, std::streampos b)
    {
        bool f = false;
        unsigned long d = 0;
        for(unsigned long i = 0; i < size; ++i)
        {
            if(m[i]->start == b)
            {
                m[i]->start = a;
                b = m[i]->end;
                if(f)
                {
                    m[d]->end = b;
                    del(i);
                }
                else
                    d = i;
                i = 0;
                f = true;
            }
            if(m[i]->end == a)
            {
                m[i]->end = b;
                a = m[i]->start;
                if(f)
                {
                    m[d]->start = a;
                    del(i);
                }
                else
                    d = i;
                i = 0;
                f = true;
            }
        }
        if(f) return;
        struct dat_table_elem** t = new dat_table_elem*[size+1];
        for(unsigned long i = 0; i < size; ++i)
            t[i] = m[i];
        delete[] m;
        t[size++] = new dat_table_elem(a, b);
        m = t;
    }

    void del(unsigned long a)
    {
        struct dat_table_elem** t = new dat_table_elem*[size-1];
        for(unsigned long i = 0; i < size; ++i)
            if(i < a)
                t[i] = m[i];
            else if(i > a)
                t[i-1] = m[i];
        delete m[a];
        delete[] m;
        m = t;
        --size;
    }
} datTable;

#endif //BIN_TREE_COURSE_PROJECT_DAT_TABLE_H
