//
// Created by rerand0m on 26.11.16.
//

#ifndef BIN_TREE_COURSE_PROJECT_NODE_H
#define BIN_TREE_COURSE_PROJECT_NODE_H

#include <iostream>

/*
 * Класс - узел бинарного дерева, будет содержать массив целых символов (не указана требуемая величина)
 * т.е. необходим шаблон для экономии памяти целые внутри массива должны быть упорядочены.
 * Каким образом тоже не указано, по этому сортировка будет происходить по возврастанию.
 * Каждый узел должен содержать:
 *  указатель на левое и правое поддерево, указатель на родителя
 *  количество целых элементов в узле
 *  сами элементы
 */

template<typename T>
class Node
{
private:
    T **mass;
    unsigned long *size;
    unsigned int dimension;

    void sort(unsigned long, unsigned long);
    unsigned int numLen(T);
    unsigned int add(char *, T, unsigned long = 0);

    char **print2(T ***m, unsigned long max);
    char **print1(T **m, unsigned long max);

    char **add_right(char **dest, char **sour);
    char **add_down(char **dest, char **sour);

    char **printN(T **m, unsigned long max, unsigned int d = 0);

    unsigned long getTheLongestElem(T **m, unsigned int d = 0);

    std::fstream& write(std::fstream&, T**, unsigned int);
    std::fstream& read(std::fstream&, T**, unsigned int);

    void deleteMass(T** m, unsigned int d);

    T* get(unsigned long, T** = nullptr, unsigned int = 0);

    void copyMass(T** m1, T** m2, unsigned int d);
public:
    std::streampos left;
    std::streampos right;
    std::streampos parent;
    unsigned int level;
    unsigned char ss;

    char* name;

    Node();
    Node(const char*);
    Node(const Node&);
    ~Node();

    unsigned long getSize(unsigned int) const;

    Node<T>& operator=(const Node<T>&);

    template<typename T1> bool operator> (Node<T1> &);
    template<typename T1> bool operator>=(Node<T1> &);
    template<typename T1> bool operator< (Node<T1> &);
    template<typename T1> bool operator<=(Node<T1> &);
    template<typename T1> bool operator==(Node<T1> &);
    template<typename T1> bool operator!=(Node<T1> &);

    char** print();

    template<typename T1>
    friend std::ostream& operator<<(std::ostream&, Node<T1>&);

    std::fstream& write(std::fstream&);
    std::fstream& read(std::fstream&);

    void sort(unsigned int);

    unsigned int getDimension();

    template<typename T1>
    void insert(T1**, unsigned long);
};

template<typename T>
template<typename T1>
void Node<T>::insert(T1 **m, unsigned long size)
{
    //Degr_MODE ON
    dimension = 1;
    if(mass)
    {
        for(decltype(size) i = 0; i < size; ++i)
            delete mass[i];
        delete[] mass;
    }
    this->size = new unsigned long[1];
    this->size[0] = size;
    mass = new T*[size];
    for(decltype(size) i = 0; i < size; ++i)
        mass[i] = new T(static_cast<T>(*m[i]));
    if(size > 0)
        sort(1);
}

template <typename T>
T* Node<T>::get(unsigned long n, T** m, unsigned int d)
{
    if(d == 0 || m == nullptr)
    {
        d = dimension;
        m = mass;
    }
    if(d == 1)
    {
        if(n < size[0]) return m[n];
        else return nullptr;
    }
    else
    {
        unsigned long a = 0, s = 1;
        unsigned int t = d-1;
        while(t--) s *= size[t];
        a = n / s;
        n = n % s;
        if(a < size[d-1]) return get(n, reinterpret_cast<T***>(m)[a], d-1);
        else return nullptr;
    }
}

template<typename T>
Node<T>::Node() :
        mass(nullptr),
        size(nullptr),
        dimension(0),
        left(0),
        right(0),
        parent(0),
        level(0),
        name(nullptr)
{}

template <typename T>
Node<T>::Node(const char* n) :
        mass(nullptr),
        size(nullptr),
        dimension(0),
        left(0),
        right(0),
        parent(0),
        level(0)
{
    unsigned long s = 0;
    while(n[s++]);
    name = new char[s];
    s = 0;
    while(n[s])
        name[s] = n[s++];
    name[s] = '\0';
}

template <typename T>
void Node<T>::copyMass(T** m1, T** m2, unsigned int d)
{
    if(d == 1)
    {
        for(unsigned long i = 0; i < size[d-1]; ++i)
            m1[i] = new T(*(m2[i]));
    }
    else
    {
        for(unsigned long i = 0; i < size[d-1]; ++i)
        {
            m1[i] = reinterpret_cast<T*>(new T*[size[d-2]]);
            copyMass(reinterpret_cast<T***>(m1)[i], reinterpret_cast<T***>(m2)[i], d-1);
        }
    }
}

template<typename T>
Node<T>::Node(const Node &a) :
    dimension(a.dimension),
    left(0),
    right(0),
    parent(0),
    level(0)
{
    size = new unsigned long[dimension];
    for(decltype(dimension) i = 0; i < dimension; ++i)
       size[i] = a.size[i];
    if(a.name)
    {
        unsigned long l = 0;
        while (a.name[l++]);
        name = new char[l];
        while (l) name[l - 1] = a.name[--l];
    }
    else
        name = nullptr;
    mass = new T*[size[dimension-1]];
    for(unsigned long i = 0; i < size[dimension-1]; ++i)
        copyMass(mass, a.mass, dimension);
}

template <typename T>
void Node<T>::deleteMass(T **m, unsigned int d)
{
    if(d == 1)
    {
        for(unsigned long i = 0; i < size[0]; ++i)
            delete m[i];
        delete[] m;
    }
    else if(d != 0)
    {
        for(unsigned long i = 0; i < size[d-1]; ++i)
            deleteMass(reinterpret_cast<T***>(m)[i], d-1);
        delete[] m;
    }
}

template<typename T>
Node<T>::~Node()
{
    deleteMass(mass, dimension);
    dimension = 0;
    left = 0;
    right = 0;
    parent = 0;
    if(size)
        delete[] size;
    if(name)
        delete[] name;
}

template<typename T>
unsigned long Node<T>::getSize(unsigned int d) const
{
    if(d < dimension)
        return size[d];
    else
        return 0;
}

template<typename T>
Node<T>& Node<T>::operator=(const Node<T> &a)
{
    if(&a == this)
        return *this;
    if(mass)
        deleteMass(mass, dimension);
    if(size)
        delete[] size;
    dimension = a.dimension;
    size = new unsigned long[dimension];
    for(decltype(dimension) i = 0; i < dimension; ++i)
        size[i] = a.size[i];
    if(a.name)
    {
        unsigned long l = 0;
        while (a.name[l++]);
        name = new char[l];
        while (l) name[l - 1] = a.name[--l];
    }
    else
        name = nullptr;
    mass = new T*[size[dimension-1]];
    for(unsigned long i = 0; i < size[dimension-1]; ++i)
        copyMass(mass, a.mass, dimension);
    return *this;
}

template <typename T>
void Node<T>::sort(unsigned long l, unsigned long r)
{
    unsigned long m = (l+r)/2;
    unsigned long oll = l, olr = r;
    while(l != m || r != m)
    {
        for(; *get(l) <= *get(m) && l < m; ++l);
        for(; *get(r) >  *get(m) && r > m; --r);
        T temp = *get(l);
        *get(l) = *get(r);
        *get(r) = temp;
        if(l == m)
            m = r;
        else if(r == m)
            m = l;
    }
    if(m > 0 && m-1 > oll)
        sort(oll, m-1);
    if(olr > (m+1))
        sort(m+1, olr);
}

template<typename T>
unsigned int Node<T>::getDimension() { return dimension; }

template<typename T>
void Node<T>::sort(unsigned int a)
{
    if(a > dimension || a == 0)
        return;
    unsigned long s  = 1, ms = 1, l = 0, r;
    {
        unsigned int t = 0;
        while (t < a) s *= size[t++];
        t = 0;
        while (t < dimension) ms *= size[t++];
    }
    r = s;
    while(r <= ms)
    {
        sort(l, r-1);
        l  = r;
        r += s;
    }
}

template<typename T>
template<typename T1>
bool Node<T>::operator>(Node<T1> &a)
{
    if(dimension == a.dimension)
    {
        for(unsigned int i = dimension; i > 0; --i)
            if(size[i - 1] != a.size[i - 1])
            {
                return size[i - 1] > a.size[i - 1];
            }
        return false;
    }
    else
    {
        return dimension > a.dimension;
    }
}

template<typename T>
template<typename T1>
bool Node<T>::operator>=(Node<T1> &a)
{
    if(dimension == a.getDimension())
    {
        for(unsigned int i = dimension; i > 0; --i)
            if(size[i - 1] != a.getSize(i - 1))
            {
                return size[i - 1] > a.getSize(i - 1);
            }
        return true;
    }
    else
    {
        return dimension >= a.getDimension();
    }
}

template<typename T>
template<typename T1>
bool Node<T>::operator<(Node<T1> &a)
{
    if(dimension == a.getDimension())
    {
        for(unsigned int i = dimension; i > 0; --i)
            if(size[i - 1] != a.getSize(i - 1))
            {
                return size[i - 1] < a.getSize(i - 1);
            }
        return false;
    }
    else
    {
        return dimension < a.getDimension();
    }
}

template<typename T>
template<typename T1>
bool Node<T>::operator<=(Node<T1> &a)
{
    if(dimension == a.getDimension())
    {
        for(unsigned int i = dimension; i > 0; --i)
            if(size[i - 1] != a.getSize(i - 1))
            {
                return size[i - 1] < a.getSize(i - 1);
            }
        return true;
    }
    else
    {
        return dimension < a.getDimension();
    }
}

template<typename T>
template<typename T1>
bool Node<T>::operator==(Node<T1> &a)
{
    if(dimension == a.getDimension())
    {
        for(unsigned int i = dimension; i > 0; --i)
            if(size[i - 1] != a.getSize(i - 1))
            {
                return false;
            }
        return true;
    }
    else
    {
        return false;
    }
}

template<typename T>
template<typename T1>
bool Node<T>::operator!=(Node<T1> &a)
{
    if(dimension == a.getDimension())
    {
        for(unsigned int i = dimension; i > 0; --i)
            if(size[i - 1] != a.getSize(i - 1))
            {
                return true;
            }
        return false;
    }
    else
    {
        return true;
    }
}

template<typename T>
unsigned int Node<T>::numLen(T n)
{
    if(n == 0) return 1;
    unsigned int ret = 0;
    if(n < 0)
    { ++ret; n *= -1; }
    while (n) { ++ret; n /= 10; }
    return ret;
}

template<typename T>
unsigned int Node<T>::add(char *p, T n, unsigned long max)
{
    unsigned int l = 0;
    if(n == 0)
    {
        p[0] = '0';
        l = 1;
    }
    else
    {
        T m = n;
        if(m < 0)
        {
            ++l;
            m *= -1;
        }
        while (m)
        {
            ++l;
            m /= 10;
        }
        auto t = l;
        bool f = false;
        if(n < 0)
        {
            p[0] = '-';
            f = true;
            n *= -1;
        }
        do
        {
            --t;
            p[t] = n % 10 + '0';
            n /= 10;
        } while (t > f);
    }
    while (l < max)
        p[l++] = ' ';
    return l;
}

template<typename T>
unsigned long Node<T>::getTheLongestElem(T **m, unsigned int d)
{
    unsigned long max = 0;
    if(d == 0)
        d = dimension;
    if(d == 1)
    {
        for(unsigned long i = 0; i < size[0]; ++i)
        {
            unsigned long t = numLen(*m[i]);
            if(t > max) max = t;
        }
    }
    else
    {
        for(unsigned long i = 0; i < size[d-1]; ++i)
        {
            unsigned long t = getTheLongestElem(reinterpret_cast<T***>(m)[i], d-1);
            if(t > max) max = t;
        }
    }
    return max;
}

template<typename T>
char** Node<T>::print()
{
    if(dimension > 0)
    {
        unsigned long max = getTheLongestElem(mass);
        if(dimension == 1)
            return print1(mass, max);
        else
            return printN(mass, max);
    }
    return nullptr;
}

template<typename T>
std::ostream& operator<<(std::ostream& stream, Node<T>& n)
{
    char** c = n.print();
    unsigned long i = 0;
    while(c && c[i][0])
        stream << c[i++] << std::endl;
    return stream;
}

template <typename T>
std::fstream& Node<T>::write(std::fstream &stream, T** m, unsigned int d)
{
    if(size)
    {
        if(d == 1)
        {
            for(unsigned long i = 0; i < size[0]; ++i)
                static_cast<std::ostream &>(stream).write(reinterpret_cast<char *>(m[i]), sizeof(T));
        }
        else
        {
            for(unsigned long i = 0; i < size[d - 1]; ++i)
                write(stream, reinterpret_cast<T ***>(m)[i], d - 1);
        }
    }
    return stream;
}

template <typename T>
std::fstream& Node<T>::write(std::fstream& stream)
{
    static_cast<std::ostream&>(stream).write(reinterpret_cast<char*>(&parent), sizeof(parent));
    static_cast<std::ostream&>(stream).write(reinterpret_cast<char*>(&left), sizeof(left));
    static_cast<std::ostream&>(stream).write(reinterpret_cast<char*>(&right), sizeof(right));
    static_cast<std::ostream&>(stream).write(reinterpret_cast<char*>(&level), sizeof(level));
    static_cast<std::ostream&>(stream).write(reinterpret_cast<char*>(&dimension), sizeof(dimension));
    for(decltype(dimension) i = 0; i < dimension; ++i)
        static_cast<std::ostream&>(stream).write(reinterpret_cast<char*>(&(size[i])), sizeof(size[i]));
    unsigned long s = 0;
    while(name[s++]);
    static_cast<std::ostream&>(stream).write(name, sizeof(char)*s);
    write(stream, mass, dimension);
    return stream;
}

template <typename T>
std::fstream& Node<T>::read(std::fstream &stream, T **m, unsigned int d)
{
    if(size)
    {
        if(d == 1)
        {
            for(unsigned long i = 0; i < size[0]; ++i)
            {
                m[i] = new T();
                static_cast<std::istream &>(stream).read(reinterpret_cast<char *>(m[i]), sizeof(T));
            }
        }
        else
        {
            for(unsigned long i = 0; i < size[d - 1]; ++i)
            {
                m[i] = reinterpret_cast<T *>(new T *[size[d - 2]]);
                read(stream, reinterpret_cast<T ***>(m)[i], d - 1);
            }

        }
    }
    return stream;
}

template <typename T>
std::fstream& Node<T>::read(std::fstream& stream)
{
    static_cast<std::istream&>(stream).read(reinterpret_cast<char*>(&parent), sizeof(parent));
    static_cast<std::istream&>(stream).read(reinterpret_cast<char*>(&left), sizeof(left));
    static_cast<std::istream&>(stream).read(reinterpret_cast<char*>(&right), sizeof(right));
    static_cast<std::istream&>(stream).read(reinterpret_cast<char*>(&level), sizeof(level));
    static_cast<std::istream&>(stream).read(reinterpret_cast<char*>(&dimension), sizeof(dimension));
    if(size)
    {
        delete[] size;
        size = nullptr;
    }
    size = new unsigned long[dimension];
    for(decltype(dimension) i = 0; i < dimension; ++i)
        static_cast<std::istream&>(stream).read(reinterpret_cast<char*>(&(size[i])), sizeof(size[i]));
    if(mass)
        deleteMass(mass, dimension);
    mass = new T*[size[dimension-1]];
    char t;
    unsigned long s = 0;
    do
    {
        static_cast<std::istream&>(stream).read(&t, sizeof(char));
        char* temp = new char[s+1];
        for(unsigned long i = 0; i < s; ++i)
            temp[i] = name[i];
        temp[s++] = t;
        delete[] name;
        name = temp;
    }while(t != '\0');
    read(stream, mass, dimension);
    return stream;
}

template<typename T>
char **Node<T>::print2(T ***m, unsigned long max)
{
    if(dimension > 1)
    {
        // x - size[0]
        // y - size[1]
        char **result = new char *[size[1] + 1];
        for(unsigned long i = 0; i < size[1]; ++i)
        {
            unsigned long pos = 0;
            result[i] = new char[size[0] * max + size[0] + 1];
            for(unsigned long j = 0; j < size[0]; ++j)
            {
                pos += add(result[i] + pos, *(m[i][j]), max);
                result[i][pos++] = ' ';
            }
            result[i][pos] = '\0';
        }
        result[size[1]] = new char('\0');
        return result;
    }
    return nullptr;
}

template<typename T>
char **Node<T>::print1(T **m, unsigned long max)
{
    char **result = new char *[2];
    unsigned long l = 0;
    while(name[l++]);
    result[0] = new char[size[0] * max + size[0] + l+1 + 1];
    result[0][size[0] * max + size[0] + l+1] = '\0';
    result[1] = new char('\0');
    unsigned long pos = 0;
    for(unsigned long i = 0; i < size[0]; ++i)
    {
        pos += add(result[0] + pos, *(m[i]), max);
        result[0][pos++] = ' ';
    }
    result[0][pos++] = ':';
    for(l = 0; name[l]; ++l)
        result[0][pos++] = name[l];
    result[0][pos] = ' ';
    return result;
}

template<typename T>
char **Node<T>::add_right(char **dest, char **sour)
{
    if(dest == nullptr)
        return sour;
    unsigned long ld = 0, ls = 0, h = 0;
    while (dest[0][ld++]);
    while (sour[0][ls++]);
    while (sour[h++][0]);

    char **result = new char *[h];
    for(decltype(h) i = 0; i < h - 1; ++i)
    {
        result[i] = new char[ls + ld + 1];
        for(decltype(ld) j = 0; j < ld - 1; ++j)
            result[i][j] = dest[i][j];
        delete[] dest[i];
        result[i][ld - 2] = '|';
        for(decltype(ls) j = 0; j < ls; ++j)
            result[i][j + ld-1] = sour[i][j];
        delete[] sour[i];
    }
    result[h - 1] = new char('\0');
    delete[] sour;
    delete[] dest;
    return result;
}

template<typename T>
char **Node<T>::add_down(char **dest, char **sour)
{
    if(dest == 0)
        return sour;
    unsigned long ld = 0, ls = 0, hd = 0, hs = 0;

    while (dest[0][ld++]);
    while (dest[hd++][0]);
    while (sour[0][ls++]);
    while (sour[hs++][0]);

    char **result = new char *[hd + hs];
    for(decltype(hd) i = 0; i < hd - 1; ++i)
    {
        result[i] = new char[(ld > ls) ? ld : ls];
        for(decltype(ld) j = 0; j < ld; ++j)
            result[i][j] = dest[i][j];
        delete[] dest[i];
    }
    result[hd - 1] = new char[(ld > ls) ? ld : ls];
    for(decltype(ls) i = 0; i < ((ls > ld) ? ls : ld); ++i)
    {
        result[hd - 1][i] = '-';
    }
    result[hd - 1][((ls > ld) ? ls : ld) - 1] = '\0';
    for(decltype(hs) i = 0; i < hs - 1; ++i)
    {
        result[i + hd] = new char[(ld > ls) ? ld : ls];
        for(decltype(ls) j = 0; j < ls; ++j)
            result[i + hd][j] = sour[i][j];
        delete[] sour[i];
    }
    result[hd + hs - 1] = new char('\0');
    delete[] sour;
    delete[] dest;
    return result;
}

template<typename T>
char **Node<T>::printN(T **m, unsigned long max, unsigned int d)
{
    if(d == 0)
    {
        d = dimension;
    }
    if(d == 2)
    {
        return print2(reinterpret_cast<T ***>(m), max);
    }
    else
    {
        char **r = nullptr;
        if(d % 2)
        {
            for(int i = 0; i < size[d - 1]; ++i)
                r = add_right(r, printN(reinterpret_cast<T ***>(m)[i], max, d - 1));
        }
        else
        {
            for(int i = 0; i < size[d - 1]; ++i)
                r = add_down(r, printN(reinterpret_cast<T ***>(m)[i], max, d - 1));
        }
        return r;
    }
}

#endif //BIN_TREE_COURSE_PROJECT_NODE_H
