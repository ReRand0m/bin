/*
 * Курсовой проект Пономаренко Романа (aka rerand0m) по программированию за 3й семестр
 * Тема 7 (разное), вариант 5
 *
 * Создать класс, производный от fstream. Файл содержит бинарное дерево. Каждая вершина дерева содержит динамический
 * массив целых переменных. Целые в дереве и массивах упорядочены.
 *
 * Формат файла: в начале его указатель на корневую вершину. Вершина содержит файловые указатели на левое и правое
 * поддеревья, количество целых значений в вершине, размерность массива, сам массив.
 *
 * Разработать полную функциональность класса. Проверить его на простых типах и сложных типах.
 * Протестировать шаблон. Программа тестирования должна содержать меню, обеспечивающее выбор операций.
 * (балл 5)
 */
#include <iostream>
#include "Node.h"
#include "BinTree.h"
#include "Mass.h"

using namespace std;

#define HELLO "___      __      ___   _____    __         ____     _____     ___    ___    _____ \n\\  \\    /  \\    /  /  |  ___|  |  |       / ___|   /  _  \\   |   \\  /   |  |  ___|\n \\  \\  /    \\  /  /   | |___   |  |      | |      |  | |  |  |    \\/    |  | |___\n  \\  \\/  /\\  \\/  /    |  ___|  |  |      | |      |  | |  |  | |\\    /| |  |  ___|\n   \\    /  \\    /     | |___   |  |___   | |___   |  |_|  |  | | \\__/ | |  | |___\n    \\__/    \\__/      |_____|  |______|   \\____|   \\_____/   |_|      |_|  |_____|\n\n\n                                      []    f[][]l\n                                    [][][]  []  []\n                                      []    []  []\n                                      []    []  []\n                                      l][]  l[][]j\n\n                                        the\n\n                                        MEGA\n ___    ___       _      _________   _____   _______   _    _\n|110\\  /010|     /1\\    |110111010| /11010\\ |1101010| \\0\\  /1/\n|1001\\/1010|    /110\\       |0|     |1|_|1|    |1|     \\1\\/0/\n|0|\\1100/|1|   /0/_\\0\\      |0|     |00110/    |0|      |01|\n|1| \\01/ |0|  /1001101\\     |1|     |0|\\1\\   __|0|__   /1/\\0\\\n|0|      |0| /1/     \\0\\    |0|     |1| \\0\\ |0001011| /1/  \\1\\  ...and binary tree\n"

char* getLine()
{
    char* res = new char[1];
    res[0] = '\0';
    unsigned long size = 1;
    char t = ' ';
    do
    {
        t = static_cast<char>(cin.get());
        if(t != '\n' && !(size == 1 && (t < '!' || t > '~')))
        {
            char *temp = new char[size + 1];
            for(decltype(size) i = 0; i < size - 1; ++i)
                temp[i] = res[i];
            delete[] res;
            temp[size - 1] = t;
            temp[size++] = '\0';
            res = temp;
        }
    }while(t != '\n' || size == 1);
    return res;
}

char* getFolder()
{
    struct stat fi;
    cout << "Введите путь к каталагу, в котором будете работать далее" << endl;
    char* home = getLine();

    unsigned long i;
    for(i = 0; home[i]; ++i);
    if(home[i-1] != '/')
    {
        char* temp = new char[i+2];
        for(i = 0; home[i]; ++i)
            temp[i] = home[i];
        delete[] home;
        temp[i++] = '/';
        temp[i] = '\0';
        home = temp;
    }

    while (stat(home, &fi) != 0)
    {
        cout << "Какие-то проблемы с этим каталогом, попробуйте другой" << endl;
        delete[] home;
        home = getLine();

        for(i = 0; home[i]; ++i);
        if(home[i-1] != '/')
        {
            char* temp = new char[i+2];
            for(i = 0; home[i]; ++i)
                temp[i] = home[i];
            delete[] home;
            temp[i++] = '/';
            temp[i] = '\0';
            home = temp;
        }
    }
    return home;
}

char* sumStr(const char* a, const char* b)
{
    unsigned long sizeA = 0, sizeB = 0;

    while(a[sizeA++]);
    while(b[sizeB++]);

    char* res = new char[sizeA + sizeB - 1];

    decltype(sizeA) i;

    for(i = 0; i < sizeA-1; ++i)
        res[i] = a[i];

    for(; i-(sizeA-1) < sizeB; ++i)
        res[i] = b[i-(sizeA-1)];
    delete[] b;
    return res;
}

bool cmpStr(const char* a, const char* b)
{
    while(*a && *b)
    {
        if(*a == *b)
        {
            a++;
            b++;
        }
        else
            return false;
    }
    return *a == '\0' && *b == '\0';
}

Mass<unsigned long>* getNumArray()
{
    struct Mass<unsigned long>* res = new struct Mass<unsigned long>();
    char t = static_cast<char>(cin.get());
    unsigned long n = 0;
    bool ts = false;
    bool m = false;
    while(t != '\n')
    {
        if(t >= '0' && t <= '9')
        {
            n = n * 10 + (t - '0');
            if(m)
            {
                n *= -1;
                m = false;
            }
            ts = true;
        }
        else if(t == '-' && !ts)
        {
            m = true;
        }
        else if(ts)
        {
            ts = false;
            res->add(n);
            n = 0;
        }
        t = static_cast<char>(cin.get());
    }
    if(ts)
        res->add(n);
    return res;
}

int main(int argc, char **argv)
{
    cout << HELLO << endl;

    /*
     *  Открыть новое дерево
     *  Закрыть дерево
     *  Создать новый узел
     *  Удалить узел
     *  Редактировать узел
     *  Найти узлы в дереве
     *  Вывести дерево на экран
     */

    char* home = getFolder();

    struct Mass<BinTree> binTrees;

    //menu
    char command = ' ';
    while(command != '0')
    {
        command = ' ';
        cout << "0 Выход" << endl
             << "1 Открыть новое дерево" << endl;
        if(binTrees.size > 0)
            cout << "2 Закрыть дерево" << endl
                 << "3 Создать новый узел" << endl
                 << "4 Удалить узел" << endl
                 << "5 Редактировать узел" << endl
                 << "6 Найти узлы в дереве" << endl
                 << "7 Вывести всё дерево на экран" << endl
                 << "8 Дефрагментация файла" << endl;
        cout << "" << endl
             << "9 Сменить рабочий каталог" << endl;
        while(command < '0' || command > '9')
            command = static_cast<char>(cin.get());
        cin.get();
        switch (command)
        {
            case '0':
            { break; }
            case '1':
            {
                cout << "Введите имя файла, в котором будет храниться (или уже хранится) дерево" << endl;
                char* file = getLine();
                file = sumStr(home, file);
                BinTree* n = new BinTree(file);
                binTrees.adda(n);
                delete[] file;
                if(!n->open_f)
                {
                    cout << "Внимание! Дерево было создано, но файл с ним не был ассоциирован из-за каких-то проблем с доступностью в данный момент" << endl
                         << "Если в данный момент у вас открыт файл, с которым вы пытались ассоциировать дерево, то для дальнейшей работы его необходимо закрыть" << endl;
                }
                break;
            }
            case '2':
            {
                cout << "Введите имя файла, с которым ассоциировано дерево" << endl;
                char* file = getLine();
                file = sumStr(home, file);
                bool f = true;
                for(unsigned long i = 0; i < binTrees.size; ++i)
                    if(cmpStr(file, binTrees[i]->getFileName()))
                    {
                        f = false;
                        binTrees.del(i);
                    }
                if(f)
                    cout << "Дерева, ассоцирумого с этим файлом не найдено" << endl;
                delete[] file;
                break;
            }
            case '3':
            {
                BinTree* btptr = nullptr;
                if(binTrees.size == 1)
                {
                    btptr = binTrees[0];
                }
                else
                {
                    cout << "Введите имя файла, с которым ассоциировано дерево, в которое хотите вставить узел" << endl;
                    char *tree = getLine();
                    tree = sumStr(home, tree);
                    for(unsigned long i = 0; i < binTrees.size; ++i)
                        if(cmpStr(binTrees[i]->getFileName(), tree))
                            btptr = binTrees[i];

                    delete[] tree;
                    if(btptr == nullptr)
                    {
                        cout << "Дерева, ассоциируемого с этим файлом не было найдено" << endl;
                        break;
                    }
                }
                cout << "Придумайте имя новому узлу" << endl;
                char* name = getLine();

                cout << "Какой тип данных будет хранится в узле?" << endl;
                cout << "1 char" << endl;
                cout << "2 unsigned char" << endl;
                cout << "3 short" << endl;
                cout << "4 unsigned short" << endl;
                cout << "5 int" << endl;
                cout << "6 unsigned int" << endl;
                cout << "7 long" << endl;
                cout << "8 unsigned long" << endl;
                char ch = static_cast<char>(cin.get());
                cin.get();
                cout << "Вводите новые значения для массива" << endl;
                Mass<unsigned long>* mass = getNumArray();
                clock_t delta = clock();
                switch (ch)
                {
                    case '1':
                    {
                        Node<char> nn = Node<char>(name);
                        nn.insert(mass->m, mass->size);
                        try
                        {
                            btptr->insert(nn);
                        }
                        catch(const char* message)
                        {
                            cerr << message << endl;
                        }
                        break;
                    }
                    case '2':
                    {
                        Node<unsigned char> nn = Node<unsigned char>(name);
                        nn.insert(mass->m, mass->size);
                        try
                        {
                            btptr->insert(nn);
                        }
                        catch(const char* message)
                        {
                            cerr << message << endl;
                        }
                        break;
                    }
                    case '3':
                    {
                        Node<short> nn = Node<short>(name);
                        nn.insert(mass->m, mass->size);
                        try
                        {
                            btptr->insert(nn);
                        }
                        catch(const char* message)
                        {
                            cerr << message << endl;
                        }
                        break;
                    }
                    case '4':
                    {
                        Node<unsigned short> nn = Node<unsigned short>(name);
                        nn.insert(mass->m, mass->size);
                        try
                        {
                            btptr->insert(nn);
                        }
                        catch(const char* message)
                        {
                            cerr << message << endl;
                        }
                        break;
                    }
                    case '5':
                    {
                        Node<int> nn = Node<int>(name);
                        nn.insert(mass->m, mass->size);
                        try
                        {
                            btptr->insert(nn);
                        }
                        catch(const char* message)
                        {
                            cerr << message << endl;
                        }
                        break;
                    }
                    case '6':
                    {
                        Node<unsigned int> nn = Node<unsigned int>(name);
                        nn.insert(mass->m, mass->size);
                        try
                        {
                            btptr->insert(nn);
                        }
                        catch(const char* message)
                        {
                            cerr << message << endl;
                        }
                        break;
                    }
                    case '7':
                    {
                        Node<long> nn = Node<long>(name);
                        nn.insert(mass->m, mass->size);
                        try
                        {
                            btptr->insert(nn);
                        }
                        catch(const char* message)
                        {
                            cerr << message << endl;
                        }
                        break;
                    }
                    case '8':
                    {
                        Node<unsigned long> nn = Node<unsigned long>(name);
                        nn.insert(mass->m, mass->size);
                        try
                        {
                            btptr->insert(nn);
                        }
                        catch(const char* message)
                        {
                            cerr << message << endl;
                        }
                        break;
                    }
                    default:
                    {
                        break;
                    }
                }
                delete   mass;
                delete[] name;
                cout << "Время вставки узла составило : " << static_cast<double>((clock() - delta)) / (CLOCKS_PER_SEC) << " 9000090009999999999999999999999999999999999999999990890щщ87с" << endl;
                break;
            }
            case '4':
            {
                BinTree* btptr = nullptr;
                if(binTrees.size == 1)
                {
                    btptr = binTrees[0];
                }
                else
                {
                    cout << "Введите имя файла, с которым ассоциировано дерево, из которого хотите удалить узел"
                         << endl;
                    char *tree = getLine();
                    tree = sumStr(home, tree);
                    for(unsigned long i = 0; i < binTrees.size; ++i)
                        if(cmpStr(binTrees[i]->getFileName(), tree))
                            btptr = binTrees[i];
                    delete[] tree;
                    if(btptr == nullptr)
                    {
                        cout << "Дерева, ассоциируемого с этим файлом не было найдено" << endl;
                        break;
                    }
                }
                cout << "Введите имя узла" << endl;
                char* name = getLine();
                try
                {
                    btptr->del(name);
                }
                catch(const char* message)
                {
                    cerr << message << endl;
                }
                delete[] name;
                break;
            }
            case '5':
            {
                BinTree* btptr = nullptr;
                if(binTrees.size == 1)
                {
                    btptr = binTrees[0];
                }
                else
                {
                    cout << "Введите имя файла, с которым ассоциировано дерево, в котором хотите редактировать узел"
                         << endl;
                    char *tree = getLine();
                    tree = sumStr(home, tree);
                    for(unsigned long i = 0; i < binTrees.size; ++i)
                        if(cmpStr(binTrees[i]->getFileName(), tree))
                            btptr = binTrees[i];
                    delete[] tree;

                    if(btptr == nullptr)
                    {
                        cout << "Дерева, ассоциируемого с этим файлом не было найдено" << endl;
                        break;
                    }
                }
                cout << "Введите имя узла" << endl;
                char* name = getLine();
                try
                {
                    cout << btptr->printOneNode(name) << endl;
                    btptr->del(name);
                }
                catch (const char* message)
                {
                    cerr << message << endl;
                    delete[] name;
                    break;
                }

                cout << "Какой тип данных будет хранится в узле?" << endl;
                cout << "1 char" << endl;
                cout << "2 unsigned char" << endl;
                cout << "3 short" << endl;
                cout << "4 unsigned short" << endl;
                cout << "5 int" << endl;
                cout << "6 unsigned int" << endl;
                cout << "7 long" << endl;
                cout << "8 unsigned long" << endl;
                char ch = static_cast<char>(cin.get());
                cin.get();
                cout << "Внимание! Узел будет удалён из дерева, и вставлен заново, все старые значения необходимо указать заново." << endl;
                cout << "Вводите новые значения для массива" << endl;
                Mass<unsigned long>* mass = getNumArray();
                switch (ch)
                {
                    case '1':
                    {
                        Node<char> nn = Node<char>(name);
                        nn.insert(mass->m, mass->size);
                        try
                        {
                            btptr->insert(nn);
                        }
                        catch(const char* message)
                        {
                            cerr << message << endl;
                            break;
                        }
                        break;
                    }
                    case '2':
                    {
                        Node<unsigned char> nn = Node<unsigned char>(name);
                        nn.insert(mass->m, mass->size);
                        try
                        {
                            btptr->insert(nn);
                        }
                        catch(const char* message)
                        {
                            cerr << message << endl;
                            break;
                        }
                        break;
                    }
                    case '3':
                    {
                        Node<short> nn = Node<short>(name);
                        nn.insert(mass->m, mass->size);
                        try
                        {
                            btptr->insert(nn);
                        }
                        catch(const char* message)
                        {
                            cerr << message << endl;
                            break;
                        }
                        break;
                    }
                    case '4':
                    {
                        Node<unsigned short> nn = Node<unsigned short>(name);
                        nn.insert(mass->m, mass->size);
                        try
                        {
                            btptr->insert(nn);
                        }
                        catch(const char* message)
                        {
                            cerr << message << endl;
                            break;
                        }
                        break;
                    }
                    case '5':
                    {
                        Node<int> nn = Node<int>(name);
                        nn.insert(mass->m, mass->size);
                        try
                        {
                            btptr->insert(nn);
                        }
                        catch(const char* message)
                        {
                            cerr << message << endl;
                            break;
                        }
                        break;
                    }
                    case '6':
                    {
                        Node<unsigned int> nn = Node<unsigned int>(name);
                        nn.insert(mass->m, mass->size);
                        try
                        {
                            btptr->insert(nn);
                        }
                        catch(const char* message)
                        {
                            cerr << message << endl;
                            break;
                        }
                        break;
                    }
                    case '7':
                    {
                        Node<long> nn = Node<long>(name);
                        nn.insert(mass->m, mass->size);
                        try
                        {
                            btptr->insert(nn);
                        }
                        catch(const char* message)
                        {
                            cerr << message << endl;
                            break;
                        }
                        break;
                    }
                    case '8':
                    {
                        Node<unsigned long> nn = Node<unsigned long>(name);
                        nn.insert(mass->m, mass->size);
                        try
                        {
                            btptr->insert(nn);
                        }
                        catch(const char* message)
                        {
                            cerr << message << endl;
                            break;
                        }
                        break;
                    }
                    default:
                    { break; }
                }
                delete   mass;
                delete[] name;

                break;
            }
            case '6':
            {
                BinTree* btptr = nullptr;
                if(binTrees.size == 1)
                {
                    btptr = binTrees[0];
                }
                else
                {
                    cout << "Введите имя файла, с которым ассоциировано дерево, в котором хотите найти узлы" << endl;

                    char *tree = getLine();
                    tree = sumStr(home, tree);
                    for(unsigned long i = 0; i < binTrees.size; ++i)
                        if(cmpStr(binTrees[i]->getFileName(), tree))
                            btptr = binTrees[i];
                    delete[] tree;

                    if(btptr == nullptr)
                    {
                        cout << "Дерева, ассоциируемого с этим файлом не было найдено" << endl;
                        break;
                    }
                }
                cout << "Введите размер узлов" << endl;
                Mass<unsigned long> *mass = getNumArray();
                unsigned long sn = *(*mass)[0];
                delete mass;

                unsigned long* size = new unsigned long(0);
                std::streampos** m;
                try
                {
                    m = btptr->find(sn, size);
                }
                catch(const char* message)
                {
                    cerr << message << endl;
                    delete size;
                    break;
                }

                for(unsigned long i = 0; i < *size; ++i)
                {
                    try
                    {
                        cout << btptr->printOneNode(*(m[i])) << endl;
                    }
                    catch(const char* message)
                    {
                        cerr << message << endl;
                    }
                }
                delete size;

                break;
            }
            case '7':
            {
                BinTree* btptr = nullptr;
                if(binTrees.size == 1)
                {
                    btptr = binTrees[0];
                }
                else
                {
                    cout << "Введите имя файла, с которым ассоциировано дерево" << endl;

                    char *tree = getLine();
                    tree = sumStr(home, tree);
                    for(unsigned long i = 0; i < binTrees.size; ++i)
                        if(cmpStr(binTrees[i]->getFileName(), tree))
                            btptr = binTrees[i];
                    delete[] tree;

                    if(btptr == nullptr)
                    {
                        cout << "Дерева, ассоциируемого с этим файлом не было найдено" << endl;
                        break;
                    }
                }
                try
                {
                    cout << btptr->print() << endl;
                }
                catch(const char* message)
                {
                    cerr << message << endl;
                }
                break;
            }
            case '8':
            {
                BinTree* btptr = nullptr;
                if(binTrees.size == 1)
                {
                    btptr = binTrees[0];
                }
                else
                {
                    cout << "Введите имя файла, с которым ассоциировано дерево" << endl;

                    char *tree = getLine();
                    tree = sumStr(home, tree);
                    for(unsigned long i = 0; i < binTrees.size; ++i)
                        if(cmpStr(binTrees[i]->getFileName(), tree))
                            btptr = binTrees[i];
                    delete[] tree;

                    if(btptr == nullptr)
                    {
                        cout << "Дерева, ассоциируемого с этим файлом не было найдено" << endl;
                        break;
                    }
                }
                try
                {
                    btptr->GC();
                }
                catch(const char* message)
                {
                    cerr << message << endl;
                }
                break;
            }
            case '9':
            {
                cout << "Внимание! Все открытые ранее файлы будут закрыты." << endl;
                while(binTrees.size)
                    binTrees.del(0);
                delete[] home;
                home = getFolder();
                break;
            }
            default:
            {
                cerr << "Команда не была распознана" << endl;
                break;
            }
        }
    }
    delete[] home;
    return 0;
}