//
// Created by rerand0m on 27.11.16.
//

#ifndef BIN_TREE_COURSE_PROJECT_BINTREE_H
#define BIN_TREE_COURSE_PROJECT_BINTREE_H

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <cstdio>

#include <iostream>
#include <fstream>
#include "Node.h"
#include "dat_table.h"

/*
 * Класс для работы с структорой бинарного дерева. Хранит только указатель на корень. Должен уметь осуществлять вставку,
 * удаление, извлечение нужных элементов по заданным параметрам, сортировка происходит при вставке. (на самом деле
 * сортировки нет, это AA дерево)
 * Меньший узел хранится в левом поддереве, больший, либо равный в правом.
 */

/*
 * file struct:
   +-------+-------+------------+
   |Bintree|rootPos|/Nodes//////|
   +-------+-------+------------+

 * Node struct:
   +---------------+--------------+------------+-------------+-----+---------+-
   |sizeOfNodesData|parent address|left address|right address|level|dimension|
   +---------------+--------------+------------+-------------+-----+---------+-
   then sizes dimension times
   -+-------+-------+-------+---+-----------------+-
    |size[0]|size[1]|size[2]|...|size[dimension-1]|
   -+-------+-------+-------+---+-----------------+-
   then name with zero byte and data
   -+----------+----------+
    |nodeName\0|///data///|
   -+----------+----------+
 */
class BinTree : public std::fstream
{
private:
    char* filename;
    char* buf;
    std::streampos** findBuf;
    unsigned long fbufSize;
    std::streampos rootPos;

    template <typename T1>
    void del(Node<T1>*, std::streampos, std::streampos = 0);

    void skew(std::streampos);
    void split(std::streampos);

    void string_add_down(char***, char**);
    void string_add_right(char***, char**);

    void load(std::streampos&, Node<void*>**);

    void laterOpening();

public:
    bool open_f;

    char* print(std::streampos = 0);

    explicit BinTree(const char*);
    ~BinTree();

    template <typename T1>
    void insert(Node<T1>&, std::streampos = 0);

    void del(char*, std::streampos = 0);
    std::streampos** find(unsigned long, unsigned long*, std::streampos** = nullptr, std::streampos = 0);

    char* printOneNode(char*, std::streampos = 0);
    char* printOneNode(std::streampos);

    char* getFileName();

    std::streampos GC(std::streampos = 0, std::fstream* = nullptr, std::streampos = 0, std::streampos* = nullptr);
    bool findGarbage(std::streampos);
};

void BinTree::laterOpening()
{
    struct stat fi;
    if(stat(filename, &fi) == 0)
    {
        open(filename, std::fstream::in);
        if(!is_open())
        {
            open_f = false;
            throw "Проблемы с открытием файла";
        }
        else
            open_f = true;
        char magic[] = "012345";
        std::fstream::read(magic, 7);
        if(magic[0] == 'B' && magic[1] == 'i' && magic[2] == 'n' && magic[3] == 'T' &&
           magic[4] == 'r' && magic[5] == 'e' && magic[6] == 'e')
        {
            try
            {
                std::fstream::read(reinterpret_cast<char *>(&rootPos), sizeof(rootPos));
            }
            catch (...)
            {
                throw "Не удалось прочесть файл. Рекомендуется прекратить работу с ним.";
            }
        }
        else
        {
            close();
            open(filename, std::fstream::out | std::fstream::trunc);
            if(!is_open())
            {
                open_f = false;
                throw "Проблемы с открытием файла";
            }
            else
                open_f = true;
            magic[0] = 'B'; magic[1] = 'i'; magic[2] = 'n'; magic[3] = 'T'; magic[4] = 'r'; magic[5] = 'e'; magic[6] = 'e';
            std::fstream::write(magic, 7);
            std::streampos null = 0;
            std::fstream::write(reinterpret_cast<char*>(&null), sizeof(null));
            close();
        }
        close();
    }
    else
    {
        open(filename, std::fstream::out);
        if(!is_open())
        {
            open_f = false;
            throw "Проблемы с открытием файла";
        }
        else
        {
            open_f = true;
        }
        char magic[] = {'B', 'i', 'n', 'T', 'r', 'e', 'e'};
        std::fstream::write(magic, 7);
        std::streampos null = 0;
        std::fstream::write(reinterpret_cast<char*>(&null), sizeof(null));
        close();
    }
}

void BinTree::load(std::streampos &a, Node<void*>** curN)
{
    if(!open_f)
    {
        laterOpening();
    }
    bool f = false;
    if(!is_open())
    {
        open(filename, std::fstream::in);
        if(!is_open())
            throw "Проблемы с открытием файла";
        f = true;
    }
    seekg(a);
    std::fstream::read(reinterpret_cast<char*>(&(*curN)->ss), sizeof((*curN)->ss));
    switch ((*curN)->ss)
    {
        case(2*sizeof(char)):
        {
            reinterpret_cast<Node<unsigned char>*>(*curN)->read(*this);
            break;
        }
        case(2*sizeof(char) + 1):
        {
            reinterpret_cast<Node<char>*>(*curN)->read(*this);
            break;
        }
        case(2*sizeof(short)):
        {
            reinterpret_cast<Node<unsigned short>*>(*curN)->read(*this);
            break;
        }
        case(2*sizeof(short)+1):
        {
            reinterpret_cast<Node<short>*>(*curN)->read(*this);
            break;
        }
        case(2*sizeof(int)):
        {
            reinterpret_cast<Node<unsigned int>*>(*curN)->read(*this);
            break;
        }
        case(2*sizeof(int)+1):
        {
            reinterpret_cast<Node<int>*>(*curN)->read(*this);
            break;
        }
        case(2*sizeof(long)):
        {
            reinterpret_cast<Node<unsigned long>*>(*curN)->read(*this);
            break;
        }
        case(2*sizeof(long)+1):
        {
            reinterpret_cast<Node<long>*>(*curN)->read(*this);
            break;
        }
        default:
        { break; }
    }
    if(f)
        close();
}

BinTree::BinTree(const char *name) : buf(nullptr), findBuf(nullptr), fbufSize(0)
{
    unsigned long len = 0;
    while (name[len++]);
    filename = new char[len];
    filename[len - 1] = '\0';
    while (len--)
        filename[len] = name[len];

    struct stat fi;
    if(stat(name, &fi) == 0)
    {
        open(filename, std::fstream::in);
        if(!is_open())
        {
            open_f = false;
        }
        else
        {
            open_f = true;
            char magic[] = "012345";
            std::fstream::read(magic, 7);
            if(magic[0] == 'B' && magic[1] == 'i' && magic[2] == 'n' && magic[3] == 'T' &&
               magic[4] == 'r' && magic[5] == 'e' && magic[6] == 'e')
            {
                std::fstream::read(reinterpret_cast<char *>(&rootPos), sizeof(rootPos));
            }
            else
            {
                close();
                open(filename, std::fstream::out | std::fstream::trunc);
                if(!is_open())
                {
                    open_f = false;
                }
                else
                {
                    open_f = true;
                    magic[0] = 'B';
                    magic[1] = 'i';
                    magic[2] = 'n';
                    magic[3] = 'T';
                    magic[4] = 'r';
                    magic[5] = 'e';
                    magic[6] = 'e';
                    std::fstream::write(magic, 7);
                    std::streampos null = 0;
                    std::fstream::write(reinterpret_cast<char *>(&null), sizeof(null));
                    close();
                }
            }
            close();
        }
    }
    else
    {
        open(filename, std::fstream::out);
        if(!is_open())
        {
            open_f = false;
        }
        else
        {
            open_f = true;

            char magic[] = {'B', 'i', 'n', 'T', 'r', 'e', 'e'};
            std::fstream::write(magic, 7);
            std::streampos null = 0;
            std::fstream::write(reinterpret_cast<char *>(&null), sizeof(null));
            close();
        }
    }
}

BinTree::~BinTree()
{
    if(is_open())
        close();
    if(buf)
    {
        delete[] buf;
        buf = nullptr;
    }
    if(findBuf)
    {
        while(fbufSize--)
            delete findBuf[fbufSize];
        delete[] findBuf;
        fbufSize = 0;
    }
    if(filename)
        delete[] filename;
}

template<typename T1>
void BinTree::insert(Node<T1> &a, std::streampos p)
{
    if(!open_f)
    {
        laterOpening();
    }
    if(p == 0) p = rootPos;
    if(rootPos == 0)
    {
        if(is_open())
            close();
        open(filename, std::fstream::out | std::fstream::in | std::fstream::ate);
        if(!is_open())
            throw "Проблемы с открытием файла";
        rootPos = tellg();
        seekg(7, std::ios_base::beg);
        std::fstream::write(reinterpret_cast<const char*>(&rootPos), sizeof(rootPos));
        seekg(rootPos);
        T1 t = -1;
        unsigned char ss = 2*sizeof(T1) + (t < 0);
        std::fstream::write(reinterpret_cast<const char*>(&ss), sizeof(ss));
        a.parent = 0;
        a.right  = 0;
        a.left   = 0;
        a.level  = 0;
        a.write(*this);
        close();
        return;
    }
    else
    {
        Node<void*>* par = new Node<void*>;
        load(p, &par);
        if(a >= *par)
        {
            if(par->right == 0)
            {
                if(is_open())
                    close();
                open(filename, std::fstream::out | std::fstream::in | std::fstream::ate);
                if(!is_open())
                    throw "Проблемы с открытием файла";
                std::streampos rr = tellp();
                a.parent = p;
                a.right  = 0;
                a.left   = 0;
                a.level  = 0;
                T1 t = -1;
                unsigned char ss = 2*sizeof(T1) + (t < 0);
                std::fstream::write(reinterpret_cast<const char*>(&ss), sizeof(ss));
                a.write(*this);
                seekg(p + static_cast<std::streampos>(sizeof(unsigned char) + 2*sizeof(std::streampos)));
                std::fstream::write(reinterpret_cast<char*>(&rr), sizeof(rr));
            }
            else
                insert(a, par->right);
        }
        else
        {
            if(par->left == 0)
            {
                if(is_open())
                    close();
                open(filename, std::fstream::out | std::fstream::in | std::fstream::ate);
                if(!is_open())
                    throw "Проблемы с открытием файла";
                std::streampos rr = tellp();
                a.parent = p;
                a.right  = 0;
                a.left   = 0;
                a.level  = 0;
                T1 t = -1;
                unsigned char ss = 2*sizeof(T1) + (t < 0);
                std::fstream::write(reinterpret_cast<const char*>(&ss), sizeof(ss));
                a.write(*this);
                seekg(p + static_cast<std::streampos>(sizeof(unsigned char) + sizeof(std::streampos)));
                std::fstream::write(reinterpret_cast<char*>(&rr), sizeof(rr));
                close();
            }
            else
            {
                if(is_open())
                    close();
                open(filename, std::fstream::in);
                if(!is_open())
                    throw "Проблемы с открытием файла";
                seekg(p + static_cast<std::streampos>(sizeof(unsigned char) + sizeof(std::streampos)));
                std::streampos np;
                std::fstream::read(reinterpret_cast<char*>(&np), sizeof(std::streampos));
                insert(a, np);
                close();
            }
        }
        skew(p);
        split(p);
        delete par;
    }
}

template<typename T1>
void BinTree::del(Node<T1> *a, std::streampos mp, std::streampos p)
{
    bool f = false;
    if(p == 0) p = rootPos;
    Node<void*> *par = new Node<void*>;
    load(p, &par);
    if(*a < *par && par->left)
        del(a, mp, par->left);
    else if(*a >= *par && par->right)
        del(a, mp, par->right);
    else f = true;

    if(f)
    {
        if(par->parent)
        {
            Node<void*>* parpar = new Node<void*>;
            load(par->parent, &parpar);
            if(parpar->left == p)
            {
                //Удаляем левого потомка
                // (аналогично) parpar->left = 0;
                seekg(par->parent + static_cast<std::streampos>(sizeof(unsigned char) + sizeof(std::streampos)), std::ios_base::beg);
                std::streampos null = 0;
                std::fstream::write(reinterpret_cast<char*>(&null), sizeof(null));
            }
            else
            {
                //Удаляем правого потомка
                // (аналогично) parpar->right = 0;
                seekg(par->parent + static_cast<std::streampos>(sizeof(unsigned char) + 2*sizeof(std::streampos)), std::ios_base::beg);
                std::streampos null = 0;
                std::fstream::write(reinterpret_cast<char*>(&null), sizeof(null));
            }
            delete parpar;
        }
        if(p == rootPos)
        {
            rootPos = 0;
            seekg(7);
            std::fstream::write(reinterpret_cast<char*>(&rootPos), sizeof(rootPos));
        }
        if(p != mp)
        {
            if(a->parent)
            {
                Node<void*>* parn = new Node<void*>;
                load(a->parent, &parn);
                if(parn->left == mp)
                {
                    parn->left = p;
                    seekg(a->parent + static_cast<std::streampos>(sizeof(unsigned char) + sizeof(std::streampos)));
                }
                else
                {
                    parn->right = p;
                    seekg(a->parent + static_cast<std::streampos>(sizeof(unsigned char) + 2*sizeof(std::streampos)));
                }
                std::fstream::write(reinterpret_cast<char*>(&p), sizeof(p));
                delete parn;
            }
            seekg(p + static_cast<std::streampos>(sizeof(unsigned char)));
            par->parent = a->parent;
            std::fstream::write(reinterpret_cast<char*>(&par->parent), sizeof(par->parent));
            if(par->left == 0 && a->left != p && a->left != 0)
            {
                par->left = a->left;
                std::fstream::write(reinterpret_cast<char*>(&par->left), sizeof(par->left));
            }
            else
                seekg(p + static_cast<std::streampos>(sizeof(unsigned char) + 2*sizeof(std::streampos)));
            if(par->right == 0 && a->right != p && a->right != 0)
            {
                par->right = a->right;
                std::fstream::write(reinterpret_cast<char*>(&par->right), sizeof(par->right));
            }
            else
                seekg(p + static_cast<std::streampos>(sizeof(unsigned char) + 3*sizeof(std::streampos)));
            par->level = a->level;
            std::fstream::write(reinterpret_cast<char*>(&par->level), sizeof(par->level));
        }
        else if(par->left)
        {
            if(a->parent)
            {
                Node<void*>* parn = new Node<void*>;
                load(a->parent, &parn);
                if(parn->left == mp)
                {
                    parn->left = par->left;
                    seekg(a->parent + static_cast<std::streampos>(sizeof(unsigned char) + sizeof(std::streampos)));
                }
                else
                {
                    parn->right = par->left;
                    seekg(a->parent + static_cast<std::streampos>(sizeof(unsigned char) + 2*sizeof(std::streampos)));
                }
                std::fstream::write(reinterpret_cast<char*>(&par->left), sizeof(par->left));
                delete parn;
            }
            seekg(par->left + static_cast<std::streampos>(sizeof(unsigned char)));
            std::fstream::write(reinterpret_cast<char*>(&a->parent), sizeof(a->parent));
            if(a->parent == 0)
            {
                rootPos = par->left;
                seekg(7);
                std::fstream::write(reinterpret_cast<char*>(&par->left), sizeof(par->left));
            }
        }
        if(mp == rootPos && p != mp)
        {
            rootPos = p;
            seekg(7);
            std::fstream::write(reinterpret_cast<char*>(&p), sizeof(p));
        }
    }
    else if(par->left || par->right)
    {
        Node<void*> *pleft = new Node<void*>, *pright = new Node<void*>;
        if(par->left)
            load(par->left, &pleft);
        if(par->right)
            load(par->right, &pright);
        if(pleft->level < par->level-1 || pright->level < par->level-1)
        {
            --(par->level);
            seekg(p + static_cast<std::streampos>(sizeof(unsigned char) + 3*sizeof(std::streampos)));
            std::fstream::write(reinterpret_cast<char*>(&par->level), sizeof(par->level));
            if(par->right && pright->level > par->level)
            {
                pright->level = par->level;
                seekg(par->right + static_cast<std::streampos>(sizeof(unsigned char) + 3*sizeof(std::streampos)));
                std::fstream::write(reinterpret_cast<char*>(&pright->level), sizeof(pright->level));
            }
            skew(p);
            if(par->right)
            {
                skew(par->right);
                if(pright->right)
                    skew(pright->right);
            }
            split(p);
            if(par->right)
                split(par->right);
        }
        delete pleft;
        delete pright;
    }
    delete par;
}
void BinTree::del(char *name, std::streampos npos)
{
    if(!open_f)
    {
        laterOpening();
    }
    if(rootPos == 0) return;
    bool f = false;
    if(!is_open())
    {
        open(filename, std::fstream::in | std::fstream::out);
        if(!is_open())
            throw "Проблемы с открытием файла";
        f = true;
    }
    if(npos == 0) npos = rootPos;
    Node<void*>* node = new Node<void*>;

    try
    {
        load(npos, &node);
    }
    catch (const char* message)
    {
        delete node;
        throw message;
    }

    char* n = name;
    char* tn = node->name;
    while(*n && *tn && *(n++) == *(tn++));
    if(*n == *tn && *(n-1) == *(tn-1))
    {
        switch (node->ss)
        {
            case(2*sizeof(char)):
            {
                del(reinterpret_cast<Node<unsigned char>*>(node), npos);
                break;
            }
            case(2*sizeof(char) + 1):
            {
                del(reinterpret_cast<Node<char>*>(node), npos);
                break;
            }
            case(2*sizeof(short)):
            {
                del(reinterpret_cast<Node<unsigned short>*>(node), npos);
                break;
            }
            case(2*sizeof(short)+1):
            {
                del(reinterpret_cast<Node<short>*>(node), npos);
                break;
            }
            case(2*sizeof(int)):
            {
                del(reinterpret_cast<Node<unsigned int>*>(node), npos);
                break;
            }
            case(2*sizeof(int)+1):
            {
                del(reinterpret_cast<Node<int>*>(node), npos);
                break;
            }
            case(2*sizeof(long)):
            {
                del(reinterpret_cast<Node<unsigned long>*>(node), npos);
                break;
            }
            case(2*sizeof(long)+1):
            {
                del(reinterpret_cast<Node<long>*>(node), npos);
                break;
            }
            default:
            { break; }
        }
    }
    else
    {
        if(node->left)
            del(name, node->left);
        if(node->right)
            del(name, node->right);
    }
    if(f)
        close();
    delete node;
}

void BinTree::skew(std::streampos p)
{
    bool f = false;
    if(!is_open())
    {
        open(filename, std::fstream::out | std::fstream::in);
        if(!is_open())
            throw "Проблемы с открытием файла";
        f = true;
    }
    Node<void*>* cur = new Node<void*>;
    load(p, &cur);
    if(cur->left)
    {
        Node<void*>* l = new Node<void *>;
        std::streampos laddr = cur->left;
        load(cur->left, &l);
        if(cur->level == l->level)
        {
            if(cur->parent)
            {
                Node<void *> *par = new Node<void *>;
                load(cur->parent, &par);
                if(par->left == p)
                {
                    par->left = cur->left;
                    seekg(cur->parent + static_cast<std::streampos>(sizeof(unsigned char) + sizeof(std::streampos)));
                    std::fstream::write(reinterpret_cast<char*>(&par->left), sizeof(par->left));
                }
                else
                {
                    par->right = cur->left;
                    seekg(cur->parent + static_cast<std::streampos>(sizeof(unsigned char) + 2*sizeof(std::streampos)));
                    std::fstream::write(reinterpret_cast<char*>(&par->right), sizeof(par->right));
                }
                delete par;
            }
            l->parent = cur->parent;
            seekg(laddr + static_cast<std::streampos>(sizeof(unsigned char)));
            std::fstream::write(reinterpret_cast<char*>(&l->parent), sizeof(l->parent));

            cur->left = l->right;
            seekg(p + static_cast<std::streampos>(sizeof(unsigned char) + sizeof(std::streampos)));
            std::fstream::write(reinterpret_cast<char*>(&cur->left), sizeof(cur->left));

            if(l->right)
            {
                //left->right->parent = p
                seekg(l->right + static_cast<std::streampos>(sizeof(unsigned char)));
                std::fstream::write(reinterpret_cast<char*>(&p), sizeof(p));
            }

            l->right = p;
            seekg(laddr + static_cast<std::streampos>(sizeof(unsigned char) + 2*sizeof(std::streampos)));
            std::fstream::write(reinterpret_cast<char*>(&l->right), sizeof(l->right));

            cur->parent = laddr;
            seekg(p + static_cast<std::streampos>(sizeof(unsigned char)));
            std::fstream::write(reinterpret_cast<char*>(&cur->parent), sizeof(cur->parent));

            if(l->parent == 0)
            {
                rootPos = laddr;
                seekg(7); //magic number
                std::fstream::write(reinterpret_cast<char*>(&rootPos), sizeof(rootPos));
            }
        }
        delete l;
    }
    delete cur;
    if(f)
        close();
}

void BinTree::split(std::streampos p)
{
    bool f = false;
    if(!is_open())
    {
        open(filename, std::fstream::out | std::fstream::in);
        if(!is_open())
            throw "Проблемы с открытием файла";
        f = true;
    }
    Node<void*>* cur = new Node<void*>;
    load(p, &cur);
    if(cur->right)
    {
        Node<void *> *r = new Node<void*>;
        load(cur->right, &r);
        std::streampos raddr = cur->right;
        if(r->right)
        {
            Node<void*>* rr = new Node<void*>;
            load(r->right, &rr);
            if(cur->level == rr->level)
            {
                if(*cur != *r)
                {
                    if(p != rootPos) // проверка на наличие родителя
                    {
                        Node<void*>* par = new Node<void*>;
                        load(cur->parent, &par);
                        if(par->left == p)
                        {
                            par->left = cur->right;
                            seekg(cur->parent + static_cast<std::streampos>(sizeof(unsigned char) + sizeof(std::streampos)));
                            std::fstream::write(reinterpret_cast<char*>(&par->left), sizeof(par->left));
                        }
                        else
                        {
                            par->right = cur->right;
                            seekg(cur->parent + static_cast<std::streampos>(sizeof(unsigned char) + 2*sizeof(std::streampos)));
                            std::fstream::write(reinterpret_cast<char*>(&par->right), sizeof(par->right));
                        }
                        delete par;
                    }
                    r->parent = cur->parent;
                    seekg(cur->right + static_cast<std::streampos>(sizeof(unsigned char)));
                    std::fstream::write(reinterpret_cast<char*>(&r->parent), sizeof(r->parent));
                    cur->right = r->left;
                    seekg(p + static_cast<std::streampos>(sizeof(unsigned char) + 2*sizeof(std::streampos)));
                    std::fstream::write(reinterpret_cast<char*>(&cur->right), sizeof(cur->right));
                    if(r->left)
                    {
                        // r->left->parent = p
                        seekg(r->left + static_cast<std::streampos>(sizeof(unsigned char)));
                        std::fstream::write(reinterpret_cast<char*>(&p), sizeof(p));
                    }
                    r->left = p;
                    seekg(raddr + static_cast<std::streampos>(sizeof(unsigned char) + sizeof(std::streampos)));
                    std::fstream::write(reinterpret_cast<char*>(&r->left), sizeof(r->left));
                    cur->parent = raddr;
                    seekg(p + static_cast<std::streampos>(sizeof(unsigned char)));
                    std::fstream::write(reinterpret_cast<char*>(&cur->parent), sizeof(cur->parent));
                    ++r->level;
                    seekg(raddr + static_cast<std::streampos>(sizeof(unsigned char) + 3*sizeof(std::streampos)));
                    std::fstream::write(reinterpret_cast<char*>(&r->level), sizeof(r->level));
                    if(r->parent == 0)
                    {
                        rootPos = raddr;
                        seekg(7); //magic number
                        std::fstream::write(reinterpret_cast<char*>(&rootPos), sizeof(rootPos));
                    }
                }
                else
                {
                    // ++r->right->level
                    unsigned int lev;
                    seekg(r->right + static_cast<std::streampos>(sizeof(unsigned char) + 3*sizeof(std::streampos)));
                    std::fstream::read(reinterpret_cast<char*>(&lev), sizeof(lev));
                    seekg(r->right + static_cast<std::streampos>(sizeof(unsigned char) + 3*sizeof(std::streampos)));
                    ++lev;
                    std::fstream::write(reinterpret_cast<char*>(&lev), sizeof(lev));
                }
            }
            delete rr;
        }
        delete r;
    }
    delete cur;
    if(f)
        close();
}

char* BinTree::print(std::streampos node)
{
    if(!open_f)
    {
        laterOpening();
    }
    if(node == 0)
        node = rootPos;
    char** inner = nullptr;
    if(rootPos == 0)
    {
        buf = new char[7];
        buf[0] = 'e'; buf[1] = 'm'; buf[2] = 'p'; buf[3] = 't'; buf[4] = 'y'; buf[5] = '\n'; buf[6] = '\0';
        return buf;
    }
    Node<void*>* cur = new Node<void*>();
    load(node, &cur);

    switch (cur->ss)
    {
        case 2*sizeof(char):
        {
            inner = reinterpret_cast<Node<unsigned char> *>(cur)->print();
            break;
        }
        case 2*sizeof(char)+1:
        {
            inner = reinterpret_cast<Node<char>*>(cur)->print();
            break;
        }
        case 2*sizeof(short):
        {
            inner = reinterpret_cast<Node<unsigned short>*>(cur)->print();
            break;
        }
        case 2*sizeof(short)+1:
        {
            inner = reinterpret_cast<Node<short>*>(cur)->print();
            break;
        }
        case 2*sizeof(int):
        {
            inner = reinterpret_cast<Node<unsigned int>*>(cur)->print();
            break;
        }
        case 2*sizeof(int)+1:
        {
            inner = reinterpret_cast<Node<int>*>(cur)->print();
            break;
        }
        case 2*sizeof(long):
        {
            inner = reinterpret_cast<Node<unsigned long>*>(cur)->print();
            break;
        }
        case 2*sizeof(long)+1:
        {
            inner = reinterpret_cast<Node<long>*>(cur)->print();
            break;
        }
        default:
        {
            inner = new char*[2];
            inner[0] = new char[5];
            inner[1] = new char('\0');
            inner[0][0] = 'f'; inner[0][1] = 'a'; inner[0][2] = 'i'; inner[0][3] = 'l'; inner[0][4] = '\0';
            break;
        }
    }
    unsigned long sizeY = 0, sizeX = 0, size = 0;
    if(inner)
        while(inner[sizeY++][0]);
    if(inner == nullptr) sizeY = 1;
    while(inner && inner[0][sizeX++]);

    char** inner_r;
    if(cur->left)
        sizeY += 3;//2*рамка и стрелка вниз
    else
        sizeY += 2;//2*рамка
    inner_r = new char*[sizeY];
    inner_r[sizeY-1] = new char('\0');

    if(cur->right == 0)
    {
        if(sizeX > 9)
        {
            inner_r[0] = new char[(sizeX - 10) * 3 + 10 + 6]; // 9 символов на адрес, 1 нуль байт, 6 на рамку
            size += (sizeX - 10) * 3 + 10 + 6;
            for(unsigned long i = 1; i < sizeY - ((cur->left)? 3 : 2); ++i)
            {
                inner_r[i] = new char[sizeX + 6];
                size += sizeX + 6;

                inner_r[i][0] = static_cast<char>('\342');
                inner_r[i][1] = static_cast<char>('\225');
                inner_r[i][2] = static_cast<char>('\221');
                for(unsigned long j = 3; j < sizeX+3-1; ++j)
                {
                    inner_r[i][j] = inner[i-1][j-3];
                }
                inner_r[i][sizeX+3-1] = static_cast<char>('\342');
                inner_r[i][sizeX+3  ] = static_cast<char>('\225');
                inner_r[i][sizeX+3+1] = static_cast<char>('\221');
                inner_r[i][sizeX+3+2] = '\0';
            }
            inner_r[sizeY-((cur->left)? 3 : 2)] = new char[(sizeX + 1) * 3 + 1]; // +2*рамка
            size += (sizeX + 1) * 3 + 1;
        }
        else
        {
            inner_r[0] = new char[9 + 2*3 + 1];
            size += 9 + 2*3 + 1;
            for(unsigned long i = 1; i < sizeY - ((cur->left)? 3 : 2); ++i)
            {
                inner_r[i] = new char[9+6+1];
                size += 9+6+1;
                inner_r[i][0] = static_cast<char>('\342');
                inner_r[i][1] = static_cast<char>('\225');
                inner_r[i][2] = static_cast<char>('\221');
                for(unsigned long j = 3; j < sizeX-1+3; ++j)
                {
                    inner_r[i][j] = inner[i-1][j-3];
                }
                for(unsigned long j = sizeX-1+3; j < 9+3; ++j)
                {
                    inner_r[i][j] = ' ';
                }
                inner_r[i][9+3] = static_cast<char>('\342');
                inner_r[i][9+4] = static_cast<char>('\225');
                inner_r[i][9+5] = static_cast<char>('\221');
                inner_r[i][9+3*2] = '\0';
            }
            inner_r[sizeY-((cur->left)? 3 : 2)] = new char[(9 + 2)*3 + 1];
            size += (9 + 2)*3 + 1;
        }
    }
    else
    {
        if(sizeX > 9)
        {
            inner_r[0] = new char[(sizeX - 9 + 1) * 3 + 9 + 3 + 1]; // 3 на стрелку
            size += (sizeX - 9+1) * 3 + 9 + 3 + 1;
            for(unsigned long i = 1; i < sizeY - ((cur->left)? 3 : 2); ++i)
            {
                inner_r[i] = new char[sizeX + 2*3 + 1];
                size += sizeX + 2*3 + 1;

                inner_r[i][0] = static_cast<char>('\342');
                inner_r[i][1] = static_cast<char>('\225');
                inner_r[i][2] = static_cast<char>('\221');
                for(unsigned long j = 3; j < sizeX+3-1; ++j)
                {
                    inner_r[i][j] = inner[i-1][j-3];
                }
                inner_r[i][sizeX+3-1] = static_cast<char>('\342');
                inner_r[i][sizeX+3  ] = static_cast<char>('\225');
                inner_r[i][sizeX+3+1] = static_cast<char>('\221');
                inner_r[i][sizeX+3+2] = ' ';
                inner_r[i][sizeX+3+3] = '\0';
            }
            inner_r[sizeY-((cur->left)? 3 : 2)] = new char[(sizeX + 1) * 3 + 2];
            size += (sizeX + 1) * 3 + 2;
        }
        else
        {
            inner_r[0] = new char[9 + 2*3 + 3 + 1];
            size += 9 + 2*3 + 3 + 1;
            for(unsigned long i = 1; i < sizeY - ((cur->left)? 3 : 2); ++i)
            {
                inner_r[i] = new char[9+6+2];
                size += 9+6+2;
                inner_r[i][0] = static_cast<char>('\342');
                inner_r[i][1] = static_cast<char>('\225');
                inner_r[i][2] = static_cast<char>('\221');
                for(unsigned long j = 3; j < sizeX-1+3; ++j)
                {
                    inner_r[i][j] = inner[i-1][j-3];
                }
                for(unsigned long j = sizeX-1+3; j < 9+3; ++j)
                {
                    inner_r[i][j] = ' ';
                }
                inner_r[i][9+3] = static_cast<char>('\342');
                inner_r[i][9+4] = static_cast<char>('\225');
                inner_r[i][9+5] = static_cast<char>('\221');
                inner_r[i][9+6] = ' ';
                inner_r[i][9+7] = '\0';
            }
            inner_r[sizeY-((cur->left)? 3 : 2)] = new char[(9 + 2)*3 + 2];
            size += (9 + 2)*3 + 2;
        }
    }
    char* last;
    if(cur->left)
    {
        inner_r[sizeY-2] = new char[((sizeX > 9)? ((cur->right)? sizeX+4 : sizeX+3) : ((cur->right)? 14 : 13))+1];
        inner_r[sizeY-2][0] = static_cast<char>('\342');
        inner_r[sizeY-2][1] = static_cast<char>('\225');
        inner_r[sizeY-2][2] = static_cast<char>('\221');
        unsigned long i = 0;
        for(; i+3 < ((sizeX > 9)? ((cur->right)? sizeX+4 : sizeX+3) : ((cur->right)? 14 : 13)); ++i)
            inner_r[sizeY-2][i+3] = ' ';
        inner_r[sizeY-2][(i++)+3] = '\0';
        size += i+3;
        last = inner_r[sizeY-3];
    }
    else
        last = inner_r[sizeY-2];

    if(cur->parent)
    {
        Node<void*>* par = new Node<void*>;
        load(cur->parent, &par);
        if(par->right == node)
        {
            inner_r[0][0] = static_cast<char>('\342');
            inner_r[0][1] = static_cast<char>('\225');
            inner_r[0][2] = static_cast<char>('\246');
        }
        else
        {
            inner_r[0][0] = static_cast<char>('\342');
            inner_r[0][1] = static_cast<char>('\225');
            inner_r[0][2] = static_cast<char>('\240');
        }
        delete par;
    }
    else
    {
        inner_r[0][0] = static_cast<char>('\342');
        inner_r[0][1] = static_cast<char>('\225');
        inner_r[0][2] = static_cast<char>('\224');
    }

    last[0] = static_cast<char>('\342');
    last[1] = static_cast<char>('\225');
    if(cur->left)
        last[2] = static_cast<char>('\240');
    else
        last[2] = static_cast<char>('\232');


    inner_r[0][3] = '0';
    inner_r[0][4] = 'x';

    decltype(sizeX) i = 5;
    for(; i < 12; ++i)
        inner_r[0][i] = (((node)>>((11-i)*4)) % 16 > 9)? static_cast<char>((((node)>>((11-i)*4)) % 16) + 'a' - 10) : static_cast<char>((((node)>>((11-i)*4)) % 16) + '0');
    if(sizeX > 9)
        for(; i-9 < (sizeX-9)*3;)
        {
            inner_r[0][i++] = static_cast<char>('\342');
            inner_r[0][i++] = static_cast<char>('\225');
            inner_r[0][i++] = static_cast<char>('\220');
        }
    inner_r[0][i++] = static_cast<char>('\342');
    inner_r[0][i++] = static_cast<char>('\225');

    if(cur->right)
    {
        inner_r[0][i++] = static_cast<char>('\246');
        inner_r[0][i++] = static_cast<char>('\342');
        inner_r[0][i++] = static_cast<char>('\225');
        inner_r[0][i++] = static_cast<char>('\220');
    }
    else
        inner_r[0][i++] = static_cast<char>('\227');

    inner_r[0][i] = '\0';

    for(i = 3; i < 3+sizeX-1; ++i)
    {
        last[3+(i-3)*3  ] = static_cast<char>('\342');
        last[3+(i-3)*3+1] = static_cast<char>('\225');
        last[3+(i-3)*3+2] = static_cast<char>('\220');
    }
    for(; i < 12; ++i)
    {
        last[3+(i-3)*3  ] = static_cast<char>('\342');
        last[3+(i-3)*3+1] = static_cast<char>('\225');
        last[3+(i-3)*3+2] = static_cast<char>('\220');
    }
    last[3+3*(i   -3)  ] = static_cast<char>('\342');
    last[3+3*(i   -3)+1] = static_cast<char>('\225');
    last[3+3*(i   -3)+2] = static_cast<char>('\235');
    i = 6+3*(i-3);
    if(cur->right)
        last[i++] = ' ';
    last[i] = '\0';

    if(cur->right)
    {
        char* n = print(cur->right);
        string_add_right(&inner_r, &n);
        unsigned long q = 0;
        size = 0;
        while(inner_r[q][0])
        {
            unsigned long w = 0;
            while(inner_r[q][w++]);
            ++q;
            size += w;
        }
    }
    if(cur->left)
    {
        char* n = print(cur->left);
        unsigned long q = 0;
        while(n[q++]);
        size+=q;
        string_add_down(&inner_r, &n);
    }

    delete cur;

    if(buf)
    {
        delete[] buf;
        buf = nullptr;
    }
    buf = new char[size];
    size = 0;
    unsigned long q = 0;
    for(; inner_r[q][0]; ++q)
    {
        for(unsigned long w = 0; inner_r[q][w]; ++w)
            buf[size++] = inner_r[q][w];
        delete[] inner_r[q];
        buf[size++] = '\n';
    }
    delete inner_r[q];
    delete[] inner_r;
    for(q = 0; inner[q][0]; ++q)
        delete[] inner[q];
    delete inner[q];
    delete[] inner;
    buf[size-1] = '\0';
    return buf;
}

void BinTree::string_add_down(char ***dest, char **sour)
{
    unsigned long s = 0, ss = 1, i = 0;
    while((*dest)[s][0]) ++s;
    for(unsigned long j = 0; (*sour)[j]; ++j)
        if((*sour)[j] == '\n')
            ++ss;
    char** t = new char*[s+ss+1];

    for(; i < s; ++i)
        t[i] = (*dest)[i];
    delete (*dest)[i];
    for(unsigned long j = 0; i-s < ss; ++i)
    {
        unsigned long q = 0;
        for(; (*sour)[j] != '\n' && (*sour)[j]; ++j) ++q;
        t[i] = new char[q+1];
        j-=q;
        for(q = 0; (*sour)[j] != '\n' && (*sour)[j]; ++j)
            t[i][q++] = (*sour)[j];
        t[i][q] = '\0';
        ++j;
    }
    t[i] = new char('\0');
    delete[] *dest;
    *dest = t;
}
void BinTree::string_add_right(char ***dest, char **sour)
{
    unsigned long s = 0, ss = 1, i = 0;
    while((*dest)[s] [0]) ++s;
    for(unsigned long j = 0; (*sour)[j]; ++j)
        if((*sour)[j] == '\n') ++ss;

    char** t = new char*[((s > ss)? s : ss) +1];
    t[(s > ss)? s : ss] = new char('\0');

    unsigned long cur = 0;
    for(; i < s; ++i)
    {
        unsigned long l_d = 0, l_s = 0;
        while((*dest)[i][l_d]) ++l_d;
        if((cur > 0 && (*sour)[cur-1]) || cur == 0)
            for(; (*sour)[l_s+cur] != '\n' && (*sour)[l_s+cur]; ++l_s);
        t[i] = new char[l_d+l_s+1];
        unsigned long j = 0;
        for(; j < l_d; ++j)
            t[i][j] = (*dest)[i][j];
        for(; j-l_d < l_s; ++j)
            t[i][j] = (*sour)[j-l_d+cur];
        t[i][j] = '\0';
        cur+=l_s + ((cur > 0 && (*sour)[cur-1]) || cur == 0);
    }
    if(i < ss)
    {
        unsigned long l = 0, fl = 0;
        while((*dest)[0][fl])
        {
            if((*dest)[0][fl] == '\342')
            { ++l; fl += 3; }
            else
            { ++l; ++fl; }
        }
        for(; i < ss; ++i)
        {
            unsigned long j = 0, l_s = 0;
            if((cur > 0 && (*sour)[cur-1]) || cur == 0)
                for(; (*sour)[l_s+cur] != '\n' && (*sour)[l_s+cur]; ++l_s);
            t[i] = new char[l_s+l+2+1];
            t[i][l_s+l+2] = '\0';
            t[i][j++] = static_cast<char>('\342');
            t[i][j++] = static_cast<char>('\225');
            t[i][j++] = static_cast<char>('\221');
            for(; j < l+2; ++j)
                t[i][j] = ' ';
            for(; j-l-2 < l_s; ++j)
                t[i][j] = (*sour)[j-l-2+cur];
            cur+=l_s + ((cur > 0 && (*sour)[cur-1]) || cur == 0);
        }
    }
    unsigned long q = 0;
    for(; (*dest)[q][0]; ++q)
    {
        delete[] (*dest)[q];
    }
    delete (*dest)[q];
    delete[] *dest;
    *dest = t;
}

std::streampos** BinTree::find(unsigned long sn, unsigned long *size, std::streampos **res, std::streampos cn)
{
    if(!open_f)
    {
        laterOpening();
    }
    if(cn == 0)
    {
        if(findBuf)
        {
            while(fbufSize--)
                delete findBuf[fbufSize];
            delete[] findBuf;
            fbufSize = 0;
        }
        cn = rootPos;
    }
    Node<void*>* cur = new Node<void*>;
    load(cn, &cur);
    if(res == nullptr)
        res = new std::streampos*[0];
    //Degr_MODE ON
    if(cur->getSize(0) == sn)
    {
        std::streampos** temp = new std::streampos*[*size+1];
        for(unsigned long i = 0; i < *size; ++i)
            temp[i] = res[i];
        temp[(*size)++] = new std::streampos(cn);
        delete[] res;
        res = temp;
        std::streampos r = cur->right;
        delete cur;
        if(r)
            res = find(sn, size, res, r);
    }
    else if(cur->getSize(0) < sn && cur->right)
    {
        auto r = cur->right;
        delete cur;
        res = find(sn, size, res, r);
    }
    else if(cur->getSize(0) > sn && cur->left)
    {
        auto l = cur->left;
        delete cur;
        res = find(sn, size, res, l);
    }
    else
        delete cur;
    findBuf = res;
    fbufSize = *size;
    return res;
}

char* BinTree::printOneNode(char *name, std::streampos node)
{
    if(!open_f)
    {
        laterOpening();
    }
    if(node == 0) node = rootPos;
    Node<void*>* cur = new Node<void*>;
    load(node, &cur);
    char* n = name;
    char* tn = cur->name;
    while(*n && *tn && *(n++) == *(tn++));
    if(*n == *tn && *(n-1) == *(tn-1))
    {
        delete cur;
        char *res = printOneNode(node);
        return res;
    }
    else
    {
        char* r = nullptr;
        std::streampos ra = cur->right, la = cur->right;
        delete cur;
        if(la)
            r = printOneNode(name, la);
        if(!r && ra)
            r = printOneNode(name, ra);
        return r;
    }
}

char* BinTree::printOneNode(std::streampos ad)
{
    if(!open_f)
    {
        laterOpening();
    }
    if(buf)
        delete[] buf;
    if(ad == 0)
        return nullptr;
    Node<void*>* n = new Node<void*>;
    load(ad, &n);
    char** r1 = nullptr;
    char* r2 = nullptr;
    switch (n->ss)
    {
        case (2*sizeof(char)):
        {
            r1 = reinterpret_cast<Node<unsigned char>*>(n)->print();
            break;
        }
        case (2*sizeof(char)+1):
        {
            r1 = reinterpret_cast<Node<char>*>(n)->print();
            break;
        }
        case (2*sizeof(short)):
        {
            r1 = reinterpret_cast<Node<unsigned short>*>(n)->print();
            break;
        }
        case (2*sizeof(short)+1):
        {
            r1 = reinterpret_cast<Node<short>*>(n)->print();
            break;
        }
        case (2*sizeof(int)):
        {
            r1 = reinterpret_cast<Node<unsigned int>*>(n)->print();
            break;
        }
        case (2*sizeof(int)+1):
        {
            r1 = reinterpret_cast<Node<int>*>(n)->print();
            break;
        }
        case (2*sizeof(long)):
        {
            r1 = reinterpret_cast<Node<unsigned long>*>(n)->print();
            break;
        }
        case (2*sizeof(long)+1):
        {
            r1 = reinterpret_cast<Node<long>*>(n)->print();
            break;
        }
        default:
            break;
    }
    long x, y, mx, offset = 0, i;
    for(y = 0; r1 && r1[y][0]; ++y);
    for(x = 0; r1 && r1[0][x]; ++x);
    mx = x+1;
    r2 = new char[(((x>9)?x:9)+7)*y+(((x>9)?x:9)+2)*3+1+(((x>9)?x:9)-9+2)*3+11+2];
    r2[offset++] = static_cast<char>('\342');
    r2[offset++] = static_cast<char>('\225');
    r2[offset++] = static_cast<char>('\224');
    r2[offset++] = '0';
    r2[offset++] = 'x';
    for(i = 0; i < 7; ++i)
        r2[offset++] = (((ad)>>((6-i)*4)) % 16 > 9)? static_cast<char>((((ad)>>((6-i)*4)) % 16) + 'a' - 10) : static_cast<char>((((ad)>>((6-i)*4)) % 16) + '0');
    for(i = 1; i < mx-9; ++i)
    {
        r2[offset++] = static_cast<char>('\342');
        r2[offset++] = static_cast<char>('\225');
        r2[offset++] = static_cast<char>('\220');
    }
    r2[offset++] = static_cast<char>('\342');
    r2[offset++] = static_cast<char>('\225');
    if(n->right)
        r2[offset++] = static_cast<char>('\246');
    else
        r2[offset++] = static_cast<char>('\227');
    r2[offset++] = '\n';
    for(y = 0; r1 && r1[y][0]; ++y)
    {
        r2[offset++] = static_cast<char>('\342');
        r2[offset++] = static_cast<char>('\225');
        r2[offset++] = static_cast<char>('\221');
        for(x = 0; r1[y][x]; ++x)
            r2[offset++] = r1[y][x];
        for(; x < 9; ++x)
            r2[offset++] = ' ';
        r2[offset++] = static_cast<char>('\342');
        r2[offset++] = static_cast<char>('\225');
        r2[offset++] = static_cast<char>('\221');
        r2[offset++] = '\n';
    }
    r2[offset++] = static_cast<char>('\342');
    r2[offset++] = static_cast<char>('\225');
    if(n->left)
        r2[offset++] = static_cast<char>('\240');
    else
        r2[offset++] = static_cast<char>('\232');
    for(i = 0; i < ((mx-1 > 9)? mx-1 : 9); ++i)
    {
        r2[offset++] = static_cast<char>('\342');
        r2[offset++] = static_cast<char>('\225');
        r2[offset++] = static_cast<char>('\220');
    }
    r2[offset++] = static_cast<char>('\342');
    r2[offset++] = static_cast<char>('\225');
    r2[offset++] = static_cast<char>('\235');
    r2[offset] = '\0';
    for(y = 0; r1 && r1[y][0]; ++y)
        delete[] r1[y];
    delete r1[y];
    delete[] r1;
    buf = r2;
    delete n;
    return r2;
}

char* BinTree::getFileName()
{
    if(buf != nullptr)
        delete[] buf;
    unsigned long i = 0;
    while(filename[i++]);
    buf = new char[i];
    for(i = 0; filename[i]; ++i)
        buf[i] = filename[i];
    buf[i] = '\0';
    return buf;
}

std::streampos BinTree::GC(std::streampos c, std::fstream* ofs, std::streampos par, std::streampos* ret)
{
    if(!open_f)
    {
        laterOpening();
    }
    bool fir = false;
    char* tf;
    std::streampos cur;
    if(c == 0)
    {
        ofs = new std::fstream();
        c = rootPos;
        unsigned long l = 0;
        while(filename[l++]);
        tf = new char[l+2];
        for(l = 0; filename[l]; ++l)
            tf[l] = filename[l];
        tf[l] = '~';
        tf[l+1] = '\0';
        ofs->open(tf, std::fstream::out | std::fstream::trunc);
        if(!ofs->is_open())
            throw "Проблемы с созданием временного файла";
        char m[] = {'B', 'i', 'n', 'T', 'r', 'e', 'e'};
        ofs->write(m, 7);
        std::streampos null = 0;
        ofs->write(reinterpret_cast<char*>(&null), sizeof(null));
        fir = true;
    }
    if(c)
    {
        Node<void *> *n = new Node<void *>;
        load(c, &n);
        cur = ofs->tellp();
        n->parent = par;
        ofs->write(reinterpret_cast<char *>(&n->ss), sizeof(n->ss));
        switch (n->ss)
        {
            case (sizeof(char)*2):
            {
                reinterpret_cast<Node<unsigned char>*>(n)->write(*ofs);
                break;
            }
            case (sizeof(char)*2+1):
            {
                reinterpret_cast<Node<char>*>(n)->write(*ofs);
                break;
            }
            case (sizeof(short)*2):
            {
                reinterpret_cast<Node<unsigned short>*>(n)->write(*ofs);
                break;
            }
            case (sizeof(short)*2+1):
            {
                reinterpret_cast<Node<short>*>(n)->write(*ofs);
                break;
            }
            case (sizeof(int)*2):
            {
                reinterpret_cast<Node<unsigned int>*>(n)->write(*ofs);
                break;
            }
            case (sizeof(int)*2+1):
            {
                reinterpret_cast<Node<int>*>(n)->write(*ofs);
                break;
            }
            case (sizeof(long)*2):
            {
                reinterpret_cast<Node<unsigned long>*>(n)->write(*ofs);
                break;
            }
            case (sizeof(long)*2+1):
            {
                reinterpret_cast<Node<long>*>(n)->write(*ofs);
                break;
            }
            default:
            {
                n->write(*ofs);
                break;
            }
        }
        std::streampos w = ofs->tellp();
        std::streampos lb = n->left, rb = n->right;
        delete n;
        if(lb)
        {
            std::streampos l = GC(lb, ofs, cur, &w);
            ofs->seekp(cur + static_cast<std::streampos>(sizeof(unsigned char) + sizeof(std::streampos)));
            ofs->write(reinterpret_cast<char *>(&l), sizeof(l));
            ofs->seekp(w);
        }
        if(rb)
        {
            std::streampos r = GC(rb, ofs, cur, &w);
            ofs->seekp(cur + static_cast<std::streampos>(sizeof(unsigned char) + 2 * sizeof(std::streampos)));
            ofs->write(reinterpret_cast<char *>(&r), sizeof(r));
            ofs->seekp(w);
        }
        if(ret)
            *ret = w;
    }
    if(fir)
    {
        ofs->seekp(7);
        ofs->write(reinterpret_cast<char *>(&cur), sizeof(cur));
        rootPos = cur;
        if(is_open())
            close();
        ofs->close();
        remove(filename);
        rename(tf, filename);
        delete[] tf;
        delete ofs;
    }
    return cur;
}

bool BinTree::findGarbage(std::streampos c)
{
    if(!open_f)
    {
        laterOpening();
    }
    if(c)
    {
        bool f = false;
        if(!is_open())
        {
            f = true;
            open(filename, std::fstream::in | std::fstream::out);
            if(!is_open())
                throw "Проблемы с открытием файла";
        }
        seekg(c + static_cast<std::streampos>(sizeof(unsigned char)));
        Node<void *> *n = new Node<void *>;
        load(c, &n);
        datTable.add(c, tellg());
        std::streampos ls = n->left, rs = n->right;
        delete n;
        if(ls)
            findGarbage(ls);
        if(rs)
            findGarbage(rs);
        if(f)
            close();
    }
    seekg(0, std::ios_base::end);
    return (datTable.size > 1) || (datTable.size == 1 && (datTable.m[0]->start > 8 || datTable.m[0]->end < tellg())) ||
            (datTable.size == 0 && tellg() > 8);
}

#endif //BIN_TREE_COURSE_PROJECT_BINTREE_H
